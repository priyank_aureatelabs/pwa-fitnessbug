import { serverHooks } from '@vue-storefront/core/server/hooks'

serverHooks.afterApplicationInitialized(({ app }) => {
  app.get('/robots.txt', (req, res) => {
    res.end('User-agent: *\nDisallow: \nSitemap: https://www.fitnessbug.co.uk/sitemap.xml')
  })
})
