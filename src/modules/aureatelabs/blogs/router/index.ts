import Blogs from '../pages/Blogs.vue'
import BlogDetail from '../pages/BlogDetail.vue'
import BlogsByTag from '../pages/BlogsByTag.vue'
import BlogSearch from '../pages/BlogSearch.vue'
export const routes = [
  { name: 'blog', path: '/blog', component: Blogs, meta: { layout: 'default', title: 'Blog' } },
  { name: 'blog-detail', path: '/blog/:slug', component: BlogDetail, meta: { layout: 'default' } },
  { name: 'blogs-by-tag', path: '/tag/:tag', component: BlogsByTag, meta: { layout: 'default' } },
  { name: 'blogs-search', path: '/blog-search', component: BlogSearch, meta: { layout: 'default' } }
]
