import { GetterTree } from 'vuex';
import BlogsState from './BlogState'
export const getters: GetterTree<BlogsState, any> = {
  getBlogsCategoryList: (state) => state.categories || [],
  getBlogList: (state) => state.posts || [],
  getBlogByIdentifier: (state) => (identifierKey) => state.posts.find(post => post.identifier === identifierKey),
  getFeaturedBlogList: (state) => state.featuredPosts,
  getBlogByPostIds: (state) => (blogIds) => state.posts.filter((post) => {
    return blogIds.filter((id) => {
      return parseInt(id) === parseInt(post.post_id) && post.is_active === true
    }).length > 0
  }),
  activeCategory: (state) => state.selectedCategory || [],
  getBlogs: (state, getters) => {
    if (getters.activeCategory.length) { // check any selected category'blog available
      let idContainer = []
      let uniqueBlogsId = null
      getters.activeCategory.forEach(id => {
        const catPostIds = getters.getBlogsCategoryList.find(category => parseInt(category.category_id) === parseInt(id))
        if (catPostIds) {
          catPostIds.posts.map(id => idContainer.push(id))
        }
      })
      uniqueBlogsId = [...new Set(idContainer)] // remove duplicate blogs id
      let finalResult = getters.getBlogList.filter(post => uniqueBlogsId.includes(post.post_id) && post.is_active === '1')
      return finalResult
    } else {
      return getters.getBlogList // return all category blogs
    }
  },
  getBlogsByTag: (state, getters) => (tag) => {
    return getters.getBlogList.filter(blog => blog.tags.includes(tag)) || []
  },
  getBlogByCurrentMonth: (state, getters) => {
    return getters.getFeaturedBlogList
  },
  getFirstBlogByPosition: (state, getters) => {
    let findFirstpositionBlog = getters.getBlogList.find(blog => blog.position === '1' && blog.include_in_recent === '0')
    return findFirstpositionBlog || getters.getFeaturedBlogList[0]
  },
  getBlogsByPosition: (state, getters) => {
    let positionedBlog = getters.getBlogList.filter(blog => blog.position !== '0' && blog.include_in_recent === '1')
    positionedBlog.sort((a, b) => parseInt(a.position) - parseInt(b.position))
    return positionedBlog || []
  }
}
