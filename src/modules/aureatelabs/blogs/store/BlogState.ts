export default interface BlogsState {
  categories: any[],
  posts: any[],
  featuredPosts: any[],
  selectedCategory: any[],
  currentPage: number,
  perPage: number
}
