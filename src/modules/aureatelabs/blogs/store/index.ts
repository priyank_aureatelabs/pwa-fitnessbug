import { Module } from 'vuex'
import actions from './actions'
import { mutations } from './mutations'
import { getters } from './getters'
import BlogsState from './BlogState'

export const blogsModule: Module<BlogsState, any> = {
  namespaced: true,
  state: {
    categories: [],
    posts: [],
    featuredPosts: [],
    selectedCategory: [],
    currentPage: 1,
    perPage: 9
  },
  mutations,
  actions,
  getters
}
