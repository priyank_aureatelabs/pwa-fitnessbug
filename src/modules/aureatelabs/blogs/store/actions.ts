import { ActionTree } from 'vuex';
import * as types from './mutation-types'
import { Logger } from '@vue-storefront/core/lib/logger'
import { quickSearchByQuery } from '@vue-storefront/core/lib/search'
import { SearchQuery } from 'storefront-query-builder'
import BlogsState from './BlogState'

const BLOG_ENTITY_NAME = 'magefan_blog_post'
const BLOG_CATEGORY_ENTITY_NAME = 'magefan_blog_category'

const actions: ActionTree<BlogsState, any> = {
  list (context, { filterValues = null, filterField = 'identifier', size = 150, start = 0, sort = '', excludeFields = null, includeFields = null, skipCache = false }) {
    let query = new SearchQuery()
    query.applyFilter({ key: 'is_active', value: { 'eq': 1 } })
    if (filterValues) {
      query = query.applyFilter({ key: filterField, value: { 'like': filterValues } })
    }
    if (skipCache || (!context.state.posts || context.state.posts.length === 0)) {
      return quickSearchByQuery({ query, size: size, sort: sort, entityType: BLOG_ENTITY_NAME, excludeFields, includeFields })
        .then((resp) => {
          context.commit(types.BLOG_FETCH_POSTS, resp.items)
          context.commit(types.BLOG_FETCH_FEATURED_POSTS)
          return resp.items
        })
        .catch(err => {
          Logger.error(err, 'blogs')()
        })
    } else {
      return new Promise((resolve, reject) => {
        let resp = context.state.posts
        resolve(resp)
      })
    }
  },

  getBlogsCategories (context, { filterValues = null, filterField = 'identifier', size = 150, start = 0, sort = '', excludeFields = null, includeFields = null, skipCache = false }) {
    let query = new SearchQuery()
    query.applyFilter({ key: 'is_active', value: { 'eq': 1 } })
    if (filterValues) {
      query = query.applyFilter({ key: filterField, value: { 'like': filterValues } })
    }
    const hasSkipCache = context.getters.getBlogsCategoryList
    if (hasSkipCache.length <= 0) {
      skipCache = true
    }
    if (skipCache || (!context.state.categories || context.state.categories.length === 0)) {
      return quickSearchByQuery({ query, size: size, sort: sort, entityType: BLOG_CATEGORY_ENTITY_NAME, excludeFields, includeFields })
        .then((resp) => {
          context.commit(types.BLOG_FETCH_CATEGORIES, resp.items)
          return resp.items
        })
        .catch(err => {
          Logger.error(err, 'blogs-categories')()
        })
    } else {
      return new Promise((resolve, reject) => {
        let resp = context.state.categories
        resolve(resp)
      })
    }
  },
  categorySelection ({ state, commit }, payload) {
    if (state.selectedCategory.includes(payload)) {
      commit(types.BLOG_REMOVE_CATS, payload) // removing category from selection
    } else {
      commit(types.BLOG_ACTIVE_CATS, payload) // adding category to selection
    }
  },
  async searchKeyword ({ commit }, searchTxt) {
    let query = {
      'query': {
        'bool': {
          'must': [{
            'term': {
              'is_active': '1'
            }
          }, {
            'multi_match': {
              'query': searchTxt,
              'type': 'phrase_prefix',
              'fields': [
                'title',
                'content'
              ]
            }
          }]
        }
      }
    };
    return quickSearchByQuery({ query, size: 500, entityType: BLOG_ENTITY_NAME })
      .then((resp) => {
        return resp.items
      })
      .catch(err => {
        Logger.error(err, 'blogs')()
      })
  }
}

export default actions
