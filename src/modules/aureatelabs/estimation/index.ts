import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import { EstimationModuleStore } from './store/index';

export const EstimationModule: StorefrontModule = function ({
  store
}) {
  store.registerModule('estimation', EstimationModuleStore)
};
