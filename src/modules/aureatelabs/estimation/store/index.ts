import { Module } from 'vuex'
import EstimationState from '../types/EstimationState'
import actions from './actions'
import { mutations } from './mutations'
import { getters } from './getters'
export const EstimationModuleStore: Module<EstimationState, any> = {
  namespaced: true,
  state: {
    estimation: []
  },
  mutations,
  actions,
  getters
}
