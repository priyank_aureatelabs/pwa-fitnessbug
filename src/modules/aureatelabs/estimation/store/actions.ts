import { ActionTree } from 'vuex';
import EstimationState from '../types/EstimationState';
import * as types from './mutation-types';
import rootStore from '@vue-storefront/core/store'
import { Logger } from '@vue-storefront/core/lib/logger';
import { adjustMultistoreApiUrl } from '@vue-storefront/core/lib/multistore'

const actions: ActionTree<EstimationState, any> = {
  async getDeliveryEstimation ({ commit }, product) {
    try {
      
      let url = rootStore.state.config.aureatelabs.deliveryEstimation.endpoint
      if (rootStore.state.config.storeViews.multistore) {
        url = adjustMultistoreApiUrl(url)
      }
      
      await fetch(url+'?sku='+product.sku, {
        method: 'GET',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json',
        }
      })
        .then(response => response.json())
        .then(data => {
          if (data.code === 200) {
            commit(types.FETCH_DELIVERY_ESTIMATION, data.result)
          } else {
            Logger.error('Something went wrong. Try again in a few seconds.', 'Estimation')()
          }
        })
    } catch (e) {
      Logger.error(e, 'Estimation')()
    }
  }
};

export default actions;
