import EstimationState from '../types/EstimationState'
import { GetterTree } from 'vuex';

export const getters: GetterTree<EstimationState, any> = {
  getEstimation: (state) => state.estimation
}
