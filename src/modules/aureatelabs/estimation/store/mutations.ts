import { MutationTree } from 'vuex'
import * as types from './mutation-types'
import Vue from 'vue'

export const mutations: MutationTree<any> = {
  [types.FETCH_DELIVERY_ESTIMATION] (state, estimation) {
    state.estimation = estimation || []
  }
}
