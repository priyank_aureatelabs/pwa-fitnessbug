import { contactUs } from './store'
import { createModule } from '@vue-storefront/core/lib/module'
import { StorefrontModule } from '@vue-storefront/core/lib/modules'

export const KEY = 'contact'
export const ContactUsModule: StorefrontModule = function ({ store, router, appConfig }) {
  store.registerModule(KEY, contactUs)
}
