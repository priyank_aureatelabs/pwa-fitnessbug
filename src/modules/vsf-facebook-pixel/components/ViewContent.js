import prepareProductObject from '../util/prepareProductObject'
import { isServer } from '@vue-storefront/core/helpers'
import { mapGetters } from 'vuex'

export default {
  computed: {
    ...mapGetters({
      product: 'product/getCurrentProduct'
    })
  },
  watch: {
    product: {
      deep: true,
      immediate: true,
      handler (product, oldProduct) {
        if (isServer) {
          return
        }
        if (!oldProduct || (product && (product.id !== oldProduct.id || product.sku !== oldProduct.sku))) {
          this.fbViewContent(product)
        }
      }
    }
  },
  methods: {
    fbViewContent (product = this.product) {
      if (typeof window !== 'undefined') { window.fbq('track', 'ViewContent', prepareProductObject(product)) }
    }
  }
}
