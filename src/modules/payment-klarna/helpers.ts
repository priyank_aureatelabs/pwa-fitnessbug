declare global {
  interface Window { _klarnaCheckout: any }
}

export const callApi = (callback: (api: any) => {}) => new Promise((resolve) => {
  if (window) {
    window._klarnaCheckout((api: any) => {
      callback(api)
      resolve()
    })
  }
})
