import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import { beforeRegistration } from './hooks/beforeRegistration'
import { afterRegistration } from './hooks/afterRegistration'
import { KlarnaModule } from './store'

const KEY = 'kco'

export const KlarnaPaymentModule: StorefrontModule = function ({ store, router, appConfig }) {
  store.registerModule(KEY, KlarnaModule)
  beforeRegistration(appConfig, store)
  afterRegistration(appConfig, store)
}
