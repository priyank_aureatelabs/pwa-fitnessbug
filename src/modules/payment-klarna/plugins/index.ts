import { KlarnaPlugin } from '../types'
import newsletter from './newsletter'
import shippingAttributes from './shippingAttributes'
import validateOrderAmount from './validateOrderAmount'
import lastOrder from './lastOrder'
import orderId from './orderId'
import savedShippingMethod from './savedShippingMethod'

export const plugins: KlarnaPlugin[] = []

export const defaultPlugins: KlarnaPlugin[] = [
  newsletter,
  shippingAttributes,
  validateOrderAmount,
  lastOrder,
  orderId,
  savedShippingMethod
]

export function addPlugin (plugin: KlarnaPlugin) {
  plugins.push(plugin)
}
