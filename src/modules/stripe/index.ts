// This is VS module entry point.
// Read more about modules: https://github.com/DivanteLtd/vue-storefront/blob/master/doc/api-modules/about-modules.md


// This key will be used for creating extension keys in vuex and other key-based plugins.
// In case of conflicting keys across modules they'll be merged in favor of the least recently registered one
import { StorefrontModule } from '@vue-storefront/core/lib/modules'
import { StripeModule } from './store'
import { beforeRegistration } from './hooks/beforeRegistration'

export const KEY = 'stripe'

export const PaymentStripeModule: StorefrontModule = function ({ store, router, appConfig }) {
  
  store.registerModule(KEY, StripeModule)
  beforeRegistration(appConfig, store)
}
