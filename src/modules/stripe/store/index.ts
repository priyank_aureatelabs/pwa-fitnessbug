import { Module } from 'vuex'
import StripeState from '../types/StripeState'

export const StripeModule: Module<StripeState, any> = {
  namespaced: true,
  state: {
    key: null
  }
}
