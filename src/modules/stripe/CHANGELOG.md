# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.5.0] - 12.11.2019

### New feature
- Compatibility with the official Stripe magento2 module - @dimasch (#20)

## [2.4.1] - 10.11.2019

### Fixed
- Added support the custom payment method code - @dimasch (#30)

## [2.4.0] - 5.11.2019

### Fixed
- Added VSF module signature for beforeRegistration hook - @dimasch (#28)