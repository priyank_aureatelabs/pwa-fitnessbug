export default {
  title: 'Fitness Bug',
  titleTemplate: '%s - Fitness Bug',
  htmlAttrs: {
    lang: 'en'
  },
  meta: [
    { charset: 'utf-8' },
    { vmid: 'description', name: 'description', content: 'FitnessBug is a sports and fitness brand with an aim to bring innovative and quality products to help its customers achieve their fitness goals' },
    { name: 'viewport', content: 'width=device-width, initial-scale=1.0' },
    { name: 'robots', content: 'index, follow' },
    { name: 'mobile-web-app-capable', content: 'yes' },
    { name: 'theme-color', content: '#000000' },
    { name: 'apple-mobile-web-app-status-bar-style', content: '#000000' }
  ],
  link: [
    { rel: 'icon', type: 'image/png', href: '/assets/favicon-32x32.png', sizes: '32x32' },
    { rel: 'icon', type: 'image/png', href: '/assets/favicon-16x16.png', sizes: '16x16' },
    { rel: 'apple-touch-icon', href: '/assets/apple-touch-icon.png' },
    { rel: 'apple-touch-startup-image', href: '/assets/apple_splash_2048.png', sizes: '2048x2732' },
    { rel: 'apple-touch-startup-image', href: '/assets/apple_splash_1668.png', sizes: '1668x2224' },
    { rel: 'apple-touch-startup-image', href: '/assets/apple_splash_1536.png', sizes: '1536x2048' },
    { rel: 'apple-touch-startup-image', href: '/assets/apple_splash_1125.png', sizes: '1125x2436' },
    { rel: 'apple-touch-startup-image', href: '/assets/apple_splash_1242.png', sizes: '1242x2208' },
    { rel: 'apple-touch-startup-image', href: '/assets/apple_splash_750.png', sizes: '750x1334' },
    { rel: 'apple-touch-startup-image', href: '/assets/apple_splash_640.png', sizes: '640x1136' },
    { rel: 'manifest', href: '/assets/manifest.json' },
    { rel: 'preload', as: 'style', href: 'https://fonts.googleapis.com/css?family=Material+Icons&display=swap', onload: 'this.onload=null;this.rel="stylesheet"' },
    { rel: 'preconnect', href: 'https://fonts.gstatic.com/', crossorigin: 'anonymous' },
    { rel: 'preconnect', href: 'https://js.stripe.com', crossorigin: '' },
    { rel: 'preconnect', href: 'https://www.paypal.com', crossorigin: '' },
    { rel: 'preconnect', href: 'https://chimpstatic.com', crossorigin: '' },
    { rel: 'preconnect', href: 'https://cdn.cookielaw.org', crossorigin: '' },
    { rel: 'preload', as: 'font', href: '/assets/fonts/AllerRegular.woff2', crossorigin: 'anonymous' },
    { rel: 'preload', as: 'font', href: '/assets/fonts/TekoRegular.woff2', crossorigin: 'anonymous' },
    { rel: 'preload', as: 'font', href: '/assets/fonts/TekoMedium.woff2', crossorigin: 'anonymous' }
  ],
  script: [
    {
      src: 'https://cdn.jsdelivr.net/npm/pwacompat@2.0.6/pwacompat.min.js',
      async: true,
      integrity: 'sha384-GOaSLecPIMCJksN83HLuYf9FToOiQ2Df0+0ntv7ey8zjUHESXhthwvq9hXAZTifA',
      crossorigin: 'anonymous'
    },
    {
      id: 'mcjs',
      type: 'text/javascript',
      innerHTML: `
      setTimeout(function(){
      !(function(c, h, i, m, p) {
        (m = c.createElement(h)),
          (p = c.getElementsByTagName(h)[0]),
          (m.defer = 1),
          (m.src = i),
          p.parentNode.insertBefore(m, p);
      })(
        document,
        'script',
        'https://chimpstatic.com/mcjs-connected/js/users/783afa2f12a273e1bb6bb6802/1261f051fae5edab57e2cc6a1.js'
      );
    }, 10000);
      `
    }
  ],
  __dangerouslyDisableSanitizers: ['script']
}
